import { v3_auth, domain } from '../config/api';

class Movie {
    static search(keyword){
        var apiSeach = domain+"/search/movie?api_key="+v3_auth+"&query="+keyword;
        return fetch(apiSeach);
    }

    static top_rated(){
        var apiTopRated = domain+"/movie/top_rated?api_key="+v3_auth;
        return fetch(apiTopRated);
    }

    static details(idFilm){
        var apiMovieDetails = domain+"/movie/"+idFilm+"?api_key="+v3_auth;
        console.log(apiMovieDetails);
        return fetch(apiMovieDetails);
    }
}

export default Movie;