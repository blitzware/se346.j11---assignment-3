import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import styles from './style';
class Search extends Component {
    render(){
        return(
            <View style={styles.container_Search}>
                <TextInput 
                    onChangeText={(text)=> this.props.onChangeKeyword(text) }
                    style={styles.input}
                    placeholder="Nhập tên phim muốn tìn"
                    placeholderTextColor="#696969"
                />
            </View>

        );
    }
}

export default Search