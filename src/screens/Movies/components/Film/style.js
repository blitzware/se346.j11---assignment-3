import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container_Film: {
        flexDirection: 'row',
        marginTop: 10,
        borderColor: '#C7C7C7',
        borderWidth: 1,
        backgroundColor: '#ffff'
    },
    container_LeftFilm: {
        flex: 1,
        padding: 10,
    },
    imageFilm: {
        flex: 1
    },
    container_RightFilm: {
        flex: 1.5,
        padding: 10,
    },
    container_nameFilm: {
        
    },
    nameFilm: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 18,
    },
    container_releaseDate: {

    },
    releaseDate: {

    },
    container_voteAverage: {

    },
    voteAverage: {

    },
    container_Overview: {
        paddingTop: 30,
        paddingBottom: 30,
    },
    overview: {

    },
});

export default styles;

