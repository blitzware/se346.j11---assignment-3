import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import sourceImage from '../../../../helper/sourceImage';
import styles from './style';

class Film extends Component {
    render(){
        return(
            <View style={styles.container_Film}>
                <View style={styles.container_LeftFilm}>
                    <Image
                        style={styles.imageFilm}
                        source={{uri: sourceImage(this.props.posterPath)}}
                    >

                    </Image>
                </View>

                <View style={styles.container_RightFilm}>

                    <View style={styles.container_nameFilm}>
                        <Text style={styles.nameFilm}>
                            { this.props.nameFilm }
                        </Text>
                    </View>
                  
                    <View style={styles.container_releaseDate}>
                        <Text style={styles.releaseDate}>
                            { this.props.releaseDate }
                        </Text>
                    </View>
                   
                    <View style={styles.container_voteAverage}>
                        <Text style={styles.voteAverage}>
                            { this.props.voteAverage }
                        </Text>
                        
                    </View>
                   
                    <View style={styles.container_Overview}>
                        <Text style={styles.overview}>
                            { this.props.overview }
                        </Text>
                    </View>

                    <View>
                        <TouchableOpacity
                           onPress={()=>this.props.clickButtonMoreInfo(this.props.idFilm)}
                        >
                            <Text> More Info </Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        );
    }
}

export default Film