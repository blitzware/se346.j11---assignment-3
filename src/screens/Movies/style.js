import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container_Movies: {
        flexDirection: 'column',
        padding: 10,
    },
    container_Search: {

    },
    container_Items: {
        marginTop: 10,
    }
})

export default styles