import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import Film from './components/Film';
import Search from './components/Search';
import styles from './style';
import Movie from '../../model/Movie';

const initState = {
    keyword: null,
    datas: null
}

class Movies extends Component {

    constructor(props){
        super(props);
        this.state = initState;
        this._clickButtonMoreInfo = this._clickButtonMoreInfo.bind(this);
        this._onChangeKeyword = this._onChangeKeyword.bind(this);
        this._loadDataTopRated = this._loadDataTopRated.bind(this);
        this._loadDataSearch = this._loadDataSearch.bind(this);
    }

    _clickButtonMoreInfo(id){
        const { navigate } = this.props.navigation;
        navigate("MovieDetailsScreen",{
            idFilm: id
        })
    }

    _onChangeKeyword(text){
        this.setState({ keyword: text })
        var keyword = this.state.keyword;
        this._loadDataSearch(keyword);
    }

    _loadDataTopRated(){
        Movie.top_rated().then((response) => response.json()).then((responseJson) => {
          this.setState({ datas: responseJson.results })
        }).catch((error) => {
          console.error(error);
        });
    }

    _loadDataSearch(keyword){
        Movie.search(keyword).then((response) => response.json()).then((responseJson) => {
            this.setState({ datas: responseJson.results })
          }).catch((error) => {
            console.error(error);
        });
    }

    componentDidMount(){
        if(this.state.keyword==null||this.state.keyword=="") 
            this._loadDataTopRated();
    }

    
    _listFilms(){
        return(
            <FlatList
                data={this.state.datas}
                renderItem={
                    (film) => <Film 
                        key={ film.item.id }
                        idFilm={ film.item.id }
                        nameFilm={ film.item.title }
                        posterPath={ film.item.poster_path }
                        releaseDate={ film.item.release_date }
                        voteAverage={ film.item.vote_average }
                        overview={ film.item.overview }
                        //Action
                        clickButtonMoreInfo={ this._clickButtonMoreInfo }
                    />
                }
                keyExtractor={(item, index) => index.toString()}
            />
        );
    }

    render(){
        if (this.state.datas==null){
            return (
                <View style={styles.container_Movies}>
                    <Text>Loading</Text>
                </View>
            );
        }else {
            return(
                <View style={styles.container_Movies}>
                    <View style={styles.container_Search}>
                        <Search 
                            onChangeKeyword={ this._onChangeKeyword }
                        />
                    </View>
                    <View style={styles.container_Items}>
                         { this._listFilms() } 
                    </View>
                </View>
            );
        }
    }
}

export default Movies