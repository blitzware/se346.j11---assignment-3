import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Movie from '../../model/Movie';

class MovieDetails extends Component {

    constructor(props){
        super(props);
        this.state = {
            datas: null,
            int: 0,
        }
        this._loadDataMovieDetails = this._loadDataMovieDetails.bind(this);
    }

    _loadDataMovieDetails(){
        const { getParam } = this.props.navigation;
        var idFilm = getParam('idFilm', 'no-id');
        Movie.details(idFilm)
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({ datas: responseJson })
          }).catch((error) => {
            console.error(error);
        });
    }

    componentDidMount(){
        this._loadDataMovieDetails(); 
        console.log(this.state.datas);
        this.setState({ int: 3 });
        console.log(this.state.int);
    }

    render(){
        
        return(
            <View>
                <Text>Xxin chao</Text>
            </View>
        );
    }
}

export default MovieDetails;