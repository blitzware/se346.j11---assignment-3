import { createStackNavigator } from 'react-navigation';
import Movies from '../screens/Movies';
import MovieDetails from '../screens/MovieDetails';

const RouterConfig = {
    MoviesScreen: { screen: Movies },
    MovieDetailsScreen: { screen: MovieDetails }
};

const StackConfig = {
    initialRouteName: 'MoviesScreen',
    navigationOptions: { header: null }
};

export default createStackNavigator(RouterConfig,StackConfig);